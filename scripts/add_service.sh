#!/bin/bash
if [ "$2" != "" ]
then
{
    head -n-1 /etc/nginx/sites-available/default
    echo $2
    tail -n-1 /etc/nginx/sites-available/default
}> /etc/nginx/sites-available/temp
mv /etc/nginx/sites-available/temp /etc/nginx/sites-available/default
fi
echo "[program:$1]
command=$3
stderr_logfile = /var/log/$1.log
stdout_logfile = /var/log/$1.log" >> /etc/supervisor/conf.d/supervisord.conf
