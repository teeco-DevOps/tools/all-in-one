#!/bin/bash
if [[ $PRELOADED != "1" ]]; then
apt install wget -y
wget "https://github.com/jpillora/cloud-torrent/releases/download/${CLOUD_TORRENT_VERSION:-0.8.25}/cloud-torrent_linux_amd64.gz"
gzip -df cloud-torrent_linux_amd64.gz
chmod +x cloud-torrent_linux_amd64
mv cloud-torrent_linux_amd64 /usr/bin/cloud-torrent
fi
/scripts/add_service.sh cloud-torrent "        location /ct {
          rewrite /ct($|/)(.*) /\$2 break;
          proxy_pass http://localhost:${CLOUD_TORRENT_PORT:-3000};
        }
        location /sync {
            proxy_set_header Cache-Control: no-cache;
            chunked_transfer_encoding off;
            proxy_cache off;
            proxy_buffering off;
            proxy_set_header Connection keep-alive;
            proxy_pass http://localhost:3000/sync;
        }" "cloud-torrent -p ${CLOUD_TORRENT_PORT:-3000}"
