apt update

apt install wget -y
wget "https://github.com/jpillora/cloud-torrent/releases/download/${CLOUD_TORRENT_VERSION:-0.8.25}/cloud-torrent_linux_amd64.gz"
gzip -df cloud-torrent_linux_amd64.gz
chmod +x cloud-torrent_linux_amd64
mv cloud-torrent_linux_amd64 /usr/bin/cloud-torrent

apt install curl -y
mkdir /opt/emby-server -p && \
 curl -fsSL -o /tmp/emby.deb \
     https://github.com/MediaBrowser/Emby.Releases/releases/download/4.8.3.0/emby-server-deb_4.8.3.0_amd64.deb && \
 dpkg-deb -xv /tmp/emby.deb /tmp/emby-server && \
 mv /tmp/emby-server/opt/emby-server/* /opt/emby-server && \
 rm /tmp/emby.deb /tmp/emby-server -rf && mkdir /var/lib/emby


apt-get -qq -y install dropbear openssh-sftp-server > /dev/null 2>&1
CONF_DIR="/etc/dropbear"
SSH_KEY_DSS="${CONF_DIR}/dropbear_dss_host_key"
SSH_KEY_RSA="${CONF_DIR}/dropbear_rsa_host_key"
rm $SSH_KEY_DSS $SSH_KEY_RSA -f;

# Check if conf dir exists
if [ ! -d ${CONF_DIR} ]; then
    mkdir -p ${CONF_DIR}
fi
chown root:root ${CONF_DIR}
chmod 755 ${CONF_DIR}

# Check if keys exists
if [ ! -f ${SSH_KEY_DSS} ]; then
    dropbearkey  -t dss -f ${SSH_KEY_DSS} > /dev/null 2>&1
fi
chown root:root ${SSH_KEY_DSS}
chmod 600 ${SSH_KEY_DSS}

if [ ! -f ${SSH_KEY_RSA} ]; then
    dropbearkey  -t rsa -f ${SSH_KEY_RSA} -s 2048 > /dev/null 2>&1
fi
chown root:root ${SSH_KEY_RSA}
chmod 600 ${SSH_KEY_RSA}

apt install qbittorrent-nox -y
mkdir /root/.config/qBittorrent/ -p
echo "[BitTorrent]
Session\Port=57062
Session\QueueingSystemEnabled=false

[LegalNotice]
Accepted=true

[Meta]
MigrationVersion=3

[Preferences]
WebUI\CSRFProtection=false
WebUI\Port=${QBITTORRENT_PORT:-9090}
WebUI\HostHeaderValidation=false" > /root/.config/qBittorrent/qBittorrent.conf

rm -rf /var/lib/apt/lists/*