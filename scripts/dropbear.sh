#!/bin/bash
if [[ $PRELOADED != "1" ]]; then
apt-get -qq -y install dropbear openssh-sftp-server > /dev/null 2>&1
CONF_DIR="/etc/dropbear"
SSH_KEY_DSS="${CONF_DIR}/dropbear_dss_host_key"
SSH_KEY_RSA="${CONF_DIR}/dropbear_rsa_host_key"
rm $SSH_KEY_DSS $SSH_KEY_RSA -f;

# Check if conf dir exists
if [ ! -d ${CONF_DIR} ]; then
    mkdir -p ${CONF_DIR}
fi
chown root:root ${CONF_DIR}
chmod 755 ${CONF_DIR}

# Check if keys exists
if [ ! -f ${SSH_KEY_DSS} ]; then
    dropbearkey  -t dss -f ${SSH_KEY_DSS} > /dev/null 2>&1
fi
chown root:root ${SSH_KEY_DSS}
chmod 600 ${SSH_KEY_DSS}

if [ ! -f ${SSH_KEY_RSA} ]; then
    dropbearkey  -t rsa -f ${SSH_KEY_RSA} -s 2048 > /dev/null 2>&1
fi
chown root:root ${SSH_KEY_RSA}
chmod 600 ${SSH_KEY_RSA}
fi
/scripts/add_service.sh dropbear "" "/usr/sbin/dropbear -E -F -p ${SSH_PORT:-22}"
# ./scripts/add_service.sh ws "        location /ws {
#             proxy_set_header Cache-Control: no-cache;
#             chunked_transfer_encoding off;
#             proxy_cache off;
#             proxy_buffering off;
#             proxy_set_header Connection keep-alive;
#             proxy_http_version 1.1;
#             proxy_set_header Upgrade \$http_upgrade;
#             proxy_set_header Connection "Upgrade";
#             proxy_set_header Host \$host;
#             proxy_pass http://localhost:${SSH_WS_PORT}/;
#         }" "exec /usr/sbin/dropbear -E -F -p ${SSH_PORT:22}"