#!/bin/bash
if [[ $PRELOADED != "1" ]]; then
apt install curl -y
mkdir /opt/emby-server -p && \
 curl -fsSL -o /tmp/emby.deb \
     https://github.com/MediaBrowser/Emby.Releases/releases/download/4.8.3.0/emby-server-deb_4.8.3.0_amd64.deb && \
 dpkg-deb -xv /tmp/emby.deb /tmp/emby-server && \
 mv /tmp/emby-server/opt/emby-server/* /opt/emby-server && \
 rm /tmp/emby.deb /tmp/emby-server -rf 
fi
/scripts/add_service.sh emby "        location / {
            proxy_set_header Cache-Control: no-cache;
            chunked_transfer_encoding off;
            proxy_cache off;
            proxy_buffering off;
            proxy_set_header Connection keep-alive;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_set_header Host \$host;
            proxy_pass http://localhost:8096/;
        }" "/opt/emby-server/bin/emby-server"