#!/bin/bash
if [[ $PRELOADED != "1" ]]; then
apt install qbittorrent-nox -y
mkdir /root/.config/qBittorrent/ -p
echo "[BitTorrent]
Session\Port=57062
Session\QueueingSystemEnabled=false

[LegalNotice]
Accepted=true

[Meta]
MigrationVersion=3

[Preferences]
WebUI\CSRFProtection=false
WebUI\Port=${QBITTORRENT_PORT:-9090}
WebUI\HostHeaderValidation=false" > /root/.config/qBittorrent/qBittorrent.conf
fi
/scripts/add_service.sh qBittorrent "                location /qb/ {
            proxy_set_header Cache-Control: no-cache;
            chunked_transfer_encoding off;
            proxy_cache off;
            proxy_buffering off;
            proxy_set_header Connection keep-alive;

            proxy_cookie_path  /                  \"/; Secure\";
            proxy_http_version 1.1;
            proxy_set_header   Host               127.0.0.1:9090;
            proxy_set_header   X-Forwarded-Host   \$http_host;
            proxy_set_header   X-Forwarded-For    \$remote_addr;
            proxy_pass http://localhost:${QBITTORRENT_PORT:-9090}/;
        }" "qbittorrent-nox"