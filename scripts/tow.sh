/scripts/add_service.sh ToW "                location /ws {
            proxy_set_header Cache-Control: no-cache;
            chunked_transfer_encoding off;
            proxy_cache off;
            proxy_buffering off;
            proxy_set_header Connection keep-alive;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_set_header Host \$host;
            proxy_pass http://localhost:${SSH_WS_PORT:-9999}/ws;
        }" "/tcp-over-websocket server -listen_ws :${SSH_WS_PORT:-9999} -connect_tcp 127.0.0.1:${SSH_PORT:-22}"
            
