FROM ubuntu
RUN apt update && apt install -y supervisor nginx && rm -rf /var/lib/apt/lists/*
# COPY config/ /var/lib/emby
COPY default.conf /etc/nginx/sites-available/default
COPY scripts/ /scripts
WORKDIR /downloads
COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY supervisord.conf /etc/supervisor/conf.d/
COPY setup.sh /setup.sh
RUN chmod +x /docker-entrypoint.sh /scripts /setup.sh -R
ENV DEBIAN_FRONTEND=noninteractive
COPY --from=registry.gitlab.com/teeco-devops/tools/tcp-over-websocket /app/tcp-over-websocket /tcp-over-websocket
RUN mkdir /var/lib/emby -p
COPY config/ /var/lib/emby
ENTRYPOINT [ "/docker-entrypoint.sh" ]
