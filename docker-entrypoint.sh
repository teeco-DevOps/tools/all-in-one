#!/bin/bash
if [[ $ENABLE_ALL == "1" ]]; then
    export ENABLE_SSH=1
    export ENABLE_EMBY=1
    export ENABLE_CLOUD_TORRENT=1
    export ENABLE_QBIT=1
fi
for var in ENABLE_SSH ENABLE_EMBY ENABLE_CLOUD_TORRENT ENABLE_QBIT; do
    if [[ "${!var}" == "1" ]]; then
        apt update
        /setup.sh
        rm -rf /var/lib/apt/lists/*
        break  # Exit the loop after running the script once
    fi
done
/scripts/tow.sh
sed -i /etc/nginx/sites-enabled/default -e "s|PORT|${PORT:-80}|"
(echo ${SSH_PASSWORD:-password}; echo ${SSH_PASSWORD:-password};) | passwd
echo Logs will appear in 10 secs && sleep 10 && tail -f /var/log/*.log &
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf -n