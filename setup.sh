#!/bin/bash
if [[ "$ENABLE_SSH" == "1" ]]
then
    /scripts/dropbear.sh
fi
if [[ "$ENABLE_EMBY" == "1" ]]
then
    /scripts/emby.sh
fi
if [[ "$ENABLE_CLOUD_TORRENT" == "1" ]]
then
    /scripts/cloud-torrent.sh
fi
if [[ "$ENABLE_QBIT" == "1" ]]
then
    /scripts/qbittorrent.sh
fi
